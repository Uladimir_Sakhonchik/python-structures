"""
Find sum of n-integer digits. n >= 0.
"""


def main():
    """Sum of number digits."""
    n = int(input())
    if n < 10:
        print(n)
    elif 10 <= n <= 100:
        print(n // 10)
    elif 100 <= n <= 999:
        print(n // 100 + n // 10 % 10 + n % 10)
    else:
        print(45)


if __name__ == "__main__":
    main()
