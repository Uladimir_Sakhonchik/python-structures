def main():
    contest = input()[::-1]
    mwords = contest.split()[::-1]
    print(" ".join(mwords))


if __name__ == "__main__":
    main()
