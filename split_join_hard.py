def main():
    contest = input()[::-1]
    mwords = list(contest)[::-1]
    new_list = []

    for i in range(len(contest)):
        if mwords[i].isupper():
            x = mwords[i].lower()
            new_list.append(x)

        elif mwords[i].islower():
            x = mwords[i].upper()
            new_list.append(x)

        elif mwords[i] == " ":
            new_list.append(mwords[i])

        else:
            new_list.append(mwords[i])

    print("".join(new_list))


if __name__ == "__main__":
    main()
