"""
 Drop empty items from a dictionary.
"""
# from ast import If
import json


def main():
    """Drop empty items from a dictionary."""
    d = json.loads(input())
    my_dict = {k: v for k, v in d.items() if v is not None}
    print(my_dict)


if __name__ == "__main__":
    main()
