"""
Calculate n!. n! = 1 * 2 * 3 * … * (n-1) * n,  0! = 1. n >= 0.
"""


def main():
    """Factorial calculation."""
    n = int(input())
    res = 1
    if n != 1:
        for i in range(2, n + 1):
            res *= i
    print(res)


if __name__ == "__main__":
    main()
