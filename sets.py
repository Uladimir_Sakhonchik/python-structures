"""
Find common items in 2 lists without duplicates. Sort the result list before output.
list_difference = []

    list_difference = []
    for element in li1:
        if element in li2:
            list_difference.append(element)

    unique_numbers = set(list_difference)
    li3 = list(unique_numbers)
    print(li3)
"""


def main():
    """Find common numbers."""
    li1 = list(map(int, input().split()))
    li2 = list(map(int, input().split()))
    print(list((set(li1) & set(li2))))


if __name__ == "__main__":
    main()
